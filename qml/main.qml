import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

ApplicationWindow {
    title: qsTr("Chess")
    visible: true
    width: 800
    height: 600
    
    property int squareSize: 70

    property var images: [
        {'imgPath' : "/images/white_pawn.svg"},
        {'imgPath' : "/images/black_pawn.svg"},
        {'imgPath' : "/images/white_rook.svg"},
        {'imgPath' : "/images/black_rook.svg"},
        {'imgPath' : "/images/white_knight.svg"},
        {'imgPath' : "/images/black_knight.svg"},
        {'imgPath' : "/images/white_bishop.svg"},
        {'imgPath' : "/images/black_bishop.svg"},
        {'imgPath' : "/images/white_queen.svg"},
        {'imgPath' : "/images/black_queen.svg"},
        {'imgPath' : "/images/white_king.svg"},
        {'imgPath' : "/images/black_king.svg"},
    ]

    Item {
      id: gameBoard
      x: 0
      y: 0
      width : logic.boardSize * squareSize
      height: logic.boardSize * squareSize
      
      Image {
        source: "/images/chess_board.jpg"
        height: gameBoard.height
        width: gameBoard.width
      }
      
      Repeater {
        model: logic

        Image {
          height: squareSize
          width : squareSize

          x: squareSize * positionX
          y: squareSize * positionY

          source: images[typeimg].imgPath
          
          MouseArea {
            anchors.fill: parent
            drag.target: parent

            property int startX: 0
            property int startY: 0

            onPressed: {
              startX = parent.x;
              startY = parent.y;
            }

            onReleased: {
              var fromX = startX / squareSize;
              var fromY = startY / squareSize;
              var toX   = (parent.x + mouseX) / squareSize;
              var toY   = (parent.y + mouseY) / squareSize;

              if (!logic.move(fromX, fromY, toX, toY)) {
                parent.x = startX;
                parent.y = startY;
              }
            }
          }
        }
      }
    }

    Column {
        anchors.left: gameBoard.right
        anchors.right: parent.right
        anchors.leftMargin: 10
        anchors.rightMargin: 10

        Button {
          id: startButton
          visible: logic.visibleNewGame

          text: "New game"

          onClicked: {
            logic.newGame();
          }
        }

        Button {
          id: loadButton
          visible: logic.visibleLoadGame

          text: "Load game"

          onClicked: {
            logic.loadGame();
          }
        }

        Button {
          id: stopButton
          visible: logic.visibleStopGame

          text: "Stop game"

          onClicked: {
            logic.stopGame();
          }
        }

        Button {
          id: saveButton
          visible: logic.visibleSaveGame

          text: "Save game"

          onClicked: {
            logic.saveGame();
          }
        }

        Button {
          id: nextMoveButton
          visible: logic.visibleNextMove
          text: "Next"

          onClicked: {
            logic.nextMove();
          }
        }

        Button {
          id: prevMoveButton
          visible: logic.visiblePrevMove

          text: "Prev"

          onClicked: {
            logic.prevMove();
          }
        }

        Text {
            text: logic.curHistoryText
            font.family: "Helvetica"
            font.pointSize: 12
            color: "black"
        }

        Text {
            text: logic.curColorText
            font.family: "Helvetica"
            font.pointSize: 18
            color: "red"
        }
    }
}
