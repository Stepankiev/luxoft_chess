#pragma once
#include <string>
#include <memory>
#include <QAbstractListModel>
#include <vector>
using namespace std;




enum PawnColor {
    colorNone = 0,
    black = 1,
    white = 2
};

enum PawnType {
    pawnNone = 0,
    pawn = 1,
    rook = 2,
    knight = 3,
    bishop = 4,
    king = 5,
    queen = 6
};


enum PawnTypeImg {
    img_white_pawn = 0,
    img_white_rook = 2,
    img_white_knight = 4,
    img_white_bishop = 6,
    img_white_queen = 8,
    img_white_king = 10,

    img_black_pawn = 1,
    img_black_rook = 3,
    img_black_knight = 5,
    img_black_bishop = 7,
    img_black_queen = 9,
    img_black_king = 11
};

struct Figure
{
    PawnColor color;
    PawnType type;
    PawnTypeImg typeimg;

    int x;
    int y;

    void setPos(int x, int y) {
        this->x = x;
        this->y = y;
    }
};

struct HistoryItem {
    int fromX;
    int fromY;
    int toX;
    int toY;
    PawnType targetPawnType = pawnNone;
    PawnColor targetPawnColor = colorNone;
    PawnTypeImg targetPawnImg;
};


class Logic: public QAbstractListModel
{
    Q_OBJECT
public:

    enum GlobalConstants {
        BOARD_SIZE = 8,
    };

    enum Roles {
        TypeImg = Qt::UserRole,
        PositionX,
        PositionY,
    };
    
public:
    explicit Logic(QObject *parent = 0);
    ~Logic();

    Q_PROPERTY(int boardSize READ boardSize CONSTANT)

    Q_PROPERTY(bool visibleNewGame READ visibleNewGame NOTIFY visibleButtonsChanged)
    Q_PROPERTY(bool visibleStopGame READ visibleStopGame NOTIFY visibleButtonsChanged)
    Q_PROPERTY(bool visibleSaveGame READ visibleSaveGame NOTIFY visibleButtonsChanged)
    Q_PROPERTY(bool visibleLoadGame READ visibleLoadGame NOTIFY visibleButtonsChanged)
    Q_PROPERTY(bool visibleNextMove READ visibleNextMove NOTIFY visibleButtonsChanged)
    Q_PROPERTY(bool visiblePrevMove READ visiblePrevMove NOTIFY visibleButtonsChanged)

    Q_PROPERTY(QString curHistoryText READ curHistoryText NOTIFY curHistoryTextChanged)
    Q_PROPERTY(QString curColorText READ curColorText NOTIFY curColorTextChanged)

    int boardSize() const;

    bool visibleNewGame();
    bool visibleStopGame();
    bool visibleSaveGame();
    bool visibleLoadGame();
    bool visibleNextMove();
    bool visiblePrevMove();
    QString curHistoryText();
    QString curColorText();
    void showCurHistoryText();
    void showCurColor();

    void nextStep();

    Q_INVOKABLE void clear();
    Q_INVOKABLE void newGame();
    Q_INVOKABLE void saveGame();
    Q_INVOKABLE void loadGame();
    Q_INVOKABLE void stopGame();
    Q_INVOKABLE bool move(int fromX, int fromY, int toX, int toY);
    Q_INVOKABLE void nextMove();
    Q_INVOKABLE void prevMove();

signals:
    void visibleButtonsChanged(bool);
    void curHistoryTextChanged();
    void curColorTextChanged();

protected:
    int rowCount(const QModelIndex & parent) const override;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

private:
    PawnColor m_curColor = white;

    bool m_visibleNewGame = false;
    bool m_visibleStopGame = false;
    bool m_visibleSaveGame = false;
    bool m_visibleLoadGame = false;
    bool m_visibleNextMove = false;
    bool m_visiblePrevMove = false;
    bool m_inLoadGame = false;
    QString  m_curHistoryText = "";
    QString  m_curColorText = "";

    vector<HistoryItem> history;
    int historyPos;

    struct Impl;
    std::unique_ptr<Impl> impl;


    void createNewBoard();
    bool moveInternal(int fromX, int fromY, int toX, int toY);
    bool canMove1(int fromX, int fromY, int toX, int toY);
    bool doMove(int fromX, int fromY, int toX, int toY);
    Figure* getFigureByXY(int x, int y);
    bool camMovePawn(Figure* figureFrom, Figure* figureTo, int toX, int toY);
    bool camMoveRook(Figure* figureFrom, Figure* figureTo, int toX, int toY);
    bool camMoveKnight(Figure* figureFrom, Figure* figureTo, int toX, int toY);
    bool camMoveBishop(Figure* figureFrom, Figure* figureTo, int toX, int toY);
    bool camMoveKing(Figure* figureFrom, Figure* figureTo, int toX, int toY);
    bool camMoveQueen(Figure* figureFrom, Figure* figureTo, int toX, int toY);
};
