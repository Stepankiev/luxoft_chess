#include "logic.h"
#include <QList>
#include <QByteArray>
#include <QHash>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;


struct Logic::Impl
{
    QList<Figure> figures;

    int findByPosition(int x, int y);
};

int Logic::Impl::findByPosition(int x, int y) {
    for (int i= 0; i < figures.size(); i++) {
        if (figures[i].x != x || figures[i].y != y ) {
            continue;
        }
        return i;
    }
    return -1;
}


Logic::Logic(QObject *parent)
    : QAbstractListModel(parent)
    , impl(new Impl())
{
    m_visibleNewGame = true;
    m_visibleStopGame = false;
    m_visibleSaveGame = false;
    m_visibleLoadGame = true;
    m_visibleNextMove = false;
    m_visiblePrevMove = false;
    visibleButtonsChanged(m_visibleNewGame);
}

Logic::~Logic() {
}

int Logic::boardSize() const {
    return BOARD_SIZE;
}

bool Logic::visibleNewGame() {
    return m_visibleNewGame;
}

bool Logic::visibleStopGame(){
    return m_visibleStopGame;
}

bool Logic::visibleSaveGame(){
    return m_visibleSaveGame;
}

bool Logic::visibleLoadGame(){
    return m_visibleLoadGame;
}

bool Logic::visibleNextMove(){
    return m_visibleNextMove;
}

bool Logic::visiblePrevMove(){
    return m_visiblePrevMove;
}

QString Logic::curHistoryText() {
    return m_curHistoryText;
}

QString Logic::curColorText() {
    return m_curColorText;
}

int Logic::rowCount(const QModelIndex & ) const {
    return impl->figures.size();
}

QHash<int, QByteArray> Logic::roleNames() const { 
    QHash<int, QByteArray> names;
    names.insert(Roles::TypeImg   , "typeimg");
    names.insert(Roles::PositionX , "positionX");
    names.insert(Roles::PositionY , "positionY");
    return names;
}

QVariant Logic::data(const QModelIndex & modelIndex, int role) const { 
    if (!modelIndex.isValid()) {
        return QVariant();
    }
    
    int index = static_cast<int>(modelIndex.row());

    if (index >= impl->figures.size() || index < 0) {
        return QVariant();
    }

    Figure & figure = impl->figures[index];
    
    switch (role) {
    case Roles::TypeImg  : return figure.typeimg;
    case Roles::PositionX: return figure.x;
    case Roles::PositionY: return figure.y;
    }
    return QVariant();
}

void Logic::createNewBoard() {
    impl->figures << Figure { black, rook,    img_black_rook,    0, 0 };
    impl->figures << Figure { black, knight,  img_black_knight,  1, 0 };
    impl->figures << Figure { black, bishop,  img_black_bishop,  2, 0 };
    impl->figures << Figure { black, queen,   img_black_queen,   3, 0 };
    impl->figures << Figure { black, king,    img_black_king,    4, 0 };
    impl->figures << Figure { black, bishop,  img_black_bishop,  5, 0 };
    impl->figures << Figure { black, knight,  img_black_knight,  6, 0 };
    impl->figures << Figure { black, rook,    img_black_rook,    7, 0 };


    impl->figures << Figure { black, pawn, img_black_pawn, 0, 1 };
    impl->figures << Figure { black, pawn, img_black_pawn, 1, 1 };
    impl->figures << Figure { black, pawn, img_black_pawn, 2, 1 };
    impl->figures << Figure { black, pawn, img_black_pawn, 3, 1 };
    impl->figures << Figure { black, pawn, img_black_pawn, 4, 1 };
    impl->figures << Figure { black, pawn, img_black_pawn, 5, 1 };
    impl->figures << Figure { black, pawn, img_black_pawn, 6, 1 };
    impl->figures << Figure { black, pawn, img_black_pawn, 7, 1 };


    impl->figures << Figure { white, pawn, img_white_pawn, 0, 6 };
    impl->figures << Figure { white, pawn, img_white_pawn, 1, 6 };
    impl->figures << Figure { white, pawn, img_white_pawn, 2, 6 };
    impl->figures << Figure { white, pawn, img_white_pawn, 3, 6 };
    impl->figures << Figure { white, pawn, img_white_pawn, 4, 6 };
    impl->figures << Figure { white, pawn, img_white_pawn, 5, 6 };
    impl->figures << Figure { white, pawn, img_white_pawn, 6, 6 };
    impl->figures << Figure { white, pawn, img_white_pawn, 7, 6 };

    impl->figures << Figure { white, rook,    img_white_rook,    0, 7 };
    impl->figures << Figure { white, knight,  img_white_knight,  1, 7 };
    impl->figures << Figure { white, bishop,  img_white_bishop,  2, 7 };
    impl->figures << Figure { white, queen,   img_white_queen,   3, 7 };
    impl->figures << Figure { white, king,    img_white_king,    4, 7 };
    impl->figures << Figure { white, bishop,  img_white_bishop,  5, 7 };
    impl->figures << Figure { white, knight,  img_white_knight,  6, 7 };
    impl->figures << Figure { white, rook,    img_white_rook,    7, 7 };
}

void Logic::clear() {
    beginResetModel();
    impl->figures.clear();
    m_curColorText = "";
    curColorTextChanged();
    endResetModel();
    history.clear();
    historyPos = 0;
}

void Logic::newGame() {
    clear();

    beginResetModel();

    createNewBoard();

    m_visibleNewGame = false;
    m_visibleStopGame = true;
    m_visibleSaveGame = true;
    m_visibleLoadGame = false;
    m_visibleNextMove = false;
    m_visiblePrevMove = false;
    m_curColor = white;
    m_inLoadGame = false;
    history.clear();
    historyPos = 0;

    visibleButtonsChanged(m_visibleNewGame);

    endResetModel();

    showCurColor();
    showCurHistoryText();
}

void Logic::stopGame() {
    clear();

    beginResetModel();

    m_visibleNewGame = true;
    m_visibleStopGame = false;
    m_visibleSaveGame = false;
    m_visibleLoadGame = true;
    m_visibleNextMove = false;
    m_visiblePrevMove = false;
    m_inLoadGame = false;
    history.clear();
    historyPos = 0;

    visibleButtonsChanged(m_visibleNewGame);

    endResetModel();
}

void Logic::saveGame() {
    ofstream myfile;
    myfile.open ("history.txt", ios::out | ios::trunc);

    for (int i=0; i<history.size(); i++) {
        HistoryItem item = history[i];

        myfile << item.fromX;
        myfile << " ";
        myfile << item.fromY;
        myfile << " ";
        myfile << item.toX;
        myfile << " ";
        myfile << item.toY;
        myfile << " ";
        myfile << item.targetPawnType;
        myfile << " ";
        myfile << item.targetPawnColor;
        myfile << " ";
        myfile << item.targetPawnImg;
        myfile << "\n";
    }

    myfile.close();
}

void Logic::loadGame() {
    clear();

    beginResetModel();

    createNewBoard();

    m_visibleNewGame = true;
    m_visibleStopGame = false;
    m_visibleSaveGame = false;
    m_visibleLoadGame = true;
    m_visibleNextMove = true;
    m_visiblePrevMove = true;
    m_inLoadGame = true;
    m_curColor = white;
    historyPos = 0;

    ifstream  myfile;
    myfile.open ("history.txt", ios::in);


    string line;
    for (line; getline(myfile, line); ) {
        int fromX;
        int fromY;
        int toX;
        int toY;
        int targetPawnType = pawnNone;
        int targetPawnColor = colorNone;
        int targetPawnImg;

        istringstream sl(line);

        sl >> fromX;
        sl >> fromY;
        sl >> toX;
        sl >> toY;
        sl >> targetPawnType;
        sl >> targetPawnColor;
        sl >> targetPawnImg;

        HistoryItem item;

        item.fromX = fromX;
        item.fromY = fromY;
        item.toX = toX;
        item.toY = toY;
        item.targetPawnType = (PawnType)targetPawnType;
        item.targetPawnColor = (PawnColor)targetPawnColor;
        item.targetPawnImg = (PawnTypeImg)targetPawnImg;

        history.push_back(item);
    }

    myfile.close();

    visibleButtonsChanged(m_visibleNewGame);

    endResetModel();

    showCurColor();
    showCurHistoryText();
}

bool Logic::canMove1(int fromX, int fromY, int toX, int toY) {
    int index = impl->findByPosition(fromX, fromY);
    if (index < 0) {
        return false;
    }

    if (toX < 0 || toX >= BOARD_SIZE || toY < 0 || toY >= BOARD_SIZE) {
        return false;
    }

    return true;
}

Figure* Logic::getFigureByXY(int x, int y) {
    Figure* figureDst = NULL;

    int indexDst = impl->findByPosition(x, y);
    if (indexDst >= 0) {
        figureDst = &impl->figures[indexDst];
    }

    return figureDst;
}

bool Logic::camMovePawn(Figure* figureFrom, Figure* figureTo, int toX, int toY) {
    if (m_curColor == white) {
        if (figureFrom->y == 6) {
            if (figureFrom->y - 1 != toY && figureFrom->y - 2 != toY) {
                return false;
            }
        } else if (figureFrom->y - 1 != toY) {
            return false;
        }
    }

    if (m_curColor == black) {
        if (figureFrom->y == 1) {
            if (figureFrom->y + 1 != toY && figureFrom->y + 2 != toY) {
                return false;
            }
        } else if (figureFrom->y + 1 != toY) {
            return false;
        }
    }

    if (figureTo != NULL) {
        if (figureTo->x == figureFrom->x + 1 || figureTo->x == figureFrom->x - 1) {
            return true;
        }

        return false;
    } else {
        if (figureFrom->x != toX) {
            return false;
        }
    }

    return true;
}

bool Logic::camMoveRook(Figure* figureFrom, Figure* figureTo, int toX, int toY) {
    for (int x = figureFrom->x+1; x<BOARD_SIZE; x++) {
        Figure* fpos = getFigureByXY(x, figureFrom->y);
        if (toX == x && toY == figureFrom->y) {
            return true;
        }
        if (fpos != NULL) {
            break;
        }
    }

    for (int x = figureFrom->x-1; x>=0; x--) {
        Figure* fpos = getFigureByXY(x, figureFrom->y);
        if (toX == x && toY == figureFrom->y) {
            return true;
        }
        if (fpos != NULL) {
            break;
        }
    }

    for (int y = figureFrom->y+1; y<BOARD_SIZE; y++) {
        Figure* fpos = getFigureByXY(figureFrom->x, y);
        if (toX == figureFrom->x && toY == y) {
            return true;
        }
        if (fpos != NULL) {
            break;
        }
    }

    for (int y = figureFrom->y-1; y>=0; y--) {
        Figure* fpos = getFigureByXY(figureFrom->x, y);
        if (toX == figureFrom->x && toY == y) {
            return true;
        }
        if (fpos != NULL) {
            break;
        }
    }

    return false;
}

bool Logic::camMoveKnight(Figure* figureFrom, Figure* figureTo, int toX, int toY) {
    if (figureFrom->x-2 == toX && figureFrom->y-1 == toY) {
        return true;
    }

    if (figureFrom->x-1 == toX && figureFrom->y-2 == toY) {
        return true;
    }

    if (figureFrom->x+2 == toX && figureFrom->y-1 == toY) {
        return true;
    }

    if (figureFrom->x+1 == toX && figureFrom->y-2 == toY) {
        return true;
    }



    if (figureFrom->x-2 == toX && figureFrom->y+1 == toY) {
        return true;
    }

    if (figureFrom->x-1 == toX && figureFrom->y+2 == toY) {
        return true;
    }

    if (figureFrom->x+2 == toX && figureFrom->y+1 == toY) {
        return true;
    }

    if (figureFrom->x+1 == toX && figureFrom->y+2 == toY) {
        return true;
    }

    return false;
}

bool Logic::camMoveBishop(Figure* figureFrom, Figure* figureTo, int toX, int toY) {
    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x + i;
        int y = figureFrom->y + i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x - i;
        int y = figureFrom->y - i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x + i;
        int y = figureFrom->y - i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x - i;
        int y = figureFrom->y + i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    return false;
}

bool Logic::camMoveKing(Figure* figureFrom, Figure* figureTo, int toX, int toY) {
    if (figureFrom->x - 1 == toX && figureFrom->y - 1 == toY) {
        return true;
    }

    if (figureFrom->x - 1 == toX && figureFrom->y + 0 == toY) {
        return true;
    }

    if (figureFrom->x - 1 == toX && figureFrom->y + 1 == toY) {
        return true;
    }

    if (figureFrom->x + 1 == toX && figureFrom->y - 1 == toY) {
        return true;
    }

    if (figureFrom->x + 1 == toX && figureFrom->y + 0 == toY) {
        return true;
    }

    if (figureFrom->x + 1 == toX && figureFrom->y + 1 == toY) {
        return true;
    }


    if (figureFrom->x + 0 == toX && figureFrom->y - 1 == toY) {
        return true;
    }

    if (figureFrom->x + 0 == toX && figureFrom->y + 1 == toY) {
        return true;
    }

    return false;
}

bool Logic::camMoveQueen(Figure* figureFrom, Figure* figureTo, int toX, int toY) {
    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x + i;
        int y = figureFrom->y + i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x - i;
        int y = figureFrom->y - i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x + i;
        int y = figureFrom->y - i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x - i;
        int y = figureFrom->y + i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x - i;
        int y = figureFrom->y;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x + i;
        int y = figureFrom->y;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x;
        int y = figureFrom->y - i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    for (int i = 1; i<BOARD_SIZE; i++) {
        int x = figureFrom->x;
        int y = figureFrom->y + i;

        if (x >= BOARD_SIZE || x < 0 || y >= BOARD_SIZE || y < 0) {
            break;
        }

        Figure* fpos = getFigureByXY(x, y);

        if (toX == x && toY == y) {
            return true;
        }

        if (fpos != NULL) {
            break;
        }
    }

    return false;
}

bool Logic::doMove(int fromX, int fromY, int toX, int toY) {
    int index = impl->findByPosition(fromX, fromY);
    Figure figureSrc = impl->figures[index];

    bool moved = true;

    int indexDst = impl->findByPosition(toX, toY);
    if (indexDst >= 0) {
        Figure figureDst = impl->figures[indexDst];

        if (figureSrc.color != figureDst.color) {
            impl->figures.removeAt(indexDst);
        } else {
            moved = false;
        }
    }

    if (moved) {
        figureSrc.setPos(toX, toY);
        index = impl->findByPosition(fromX, fromY);
        impl->figures[index] = figureSrc;
    }

    return moved;
}

bool Logic::moveInternal(int fromX, int fromY, int toX, int toY) {
    bool canMove = canMove1(fromX, fromY, toX, toY);

    Figure* figureFrom = getFigureByXY(fromX, fromY);
    Figure* figureTo = getFigureByXY(toX, toY);

    if (figureFrom == NULL) {
        canMove = false;
    }

    if (canMove) {
        if (figureTo != NULL) {
            if (figureFrom->color == figureTo->color) {
                canMove = false;
            }
        }
    }

    if (canMove) {
        if (figureFrom->color != m_curColor) {
            canMove = false;
        }
    }

    if (canMove) {
        if (figureFrom->type == pawn) {
            canMove = camMovePawn(figureFrom, figureTo, toX, toY);
        }
        if (figureFrom->type == rook) {
            canMove = camMoveRook(figureFrom, figureTo, toX, toY);
        }
        if (figureFrom->type == knight) {
            canMove = camMoveKnight(figureFrom, figureTo, toX, toY);
        }
        if (figureFrom->type == bishop) {
            canMove = camMoveBishop(figureFrom, figureTo, toX, toY);
        }
        if (figureFrom->type == king) {
            canMove = camMoveKing(figureFrom, figureTo, toX, toY);
        }
        if (figureFrom->type == queen) {
            canMove = camMoveQueen(figureFrom, figureTo, toX, toY);
        }
    }

    bool moved = false;

    if (canMove) {
        beginResetModel();

        moved = doMove(fromX, fromY, toX, toY);

        endResetModel();
    }

    if (moved) {
        nextStep();
    }

    return moved;
}

bool Logic::move(int fromX, int fromY, int toX, int toY) {
    if (m_inLoadGame) {
        return false;
    }

    Figure* figureTo = getFigureByXY(toX, toY);

    HistoryItem item;
    if (figureTo != NULL) {
        item.targetPawnColor = figureTo->color;
        item.targetPawnType = figureTo->type;
        item.targetPawnImg = figureTo->typeimg;
    }

    bool moved = moveInternal(fromX, fromY, toX, toY);

    if (moved) {
        item.fromX = fromX;
        item.fromY = fromY;
        item.toX = toX;
        item.toY = toY;
        history.push_back(item);
    }

    return moved;
}

void Logic::showCurHistoryText() {
    beginResetModel();
    if (m_inLoadGame) {
        m_curHistoryText = "";
        m_curHistoryText = "History pos: " + QString::number(historyPos) + " of " + QString::number(history.size()) + ".";
    } else {
        m_curHistoryText = "";
    }
    curHistoryTextChanged();
    endResetModel();
}

void Logic::showCurColor() {
    beginResetModel();
    if (m_curColor == white) {
        m_curColorText = "Current move white";
    } else {
        m_curColorText = "Current move black";
    }
    curColorTextChanged();
    endResetModel();
}

void Logic::nextStep() {
    if (m_curColor == white) {
        m_curColor = black;
    } else {
        m_curColor = white;
    }
    showCurColor();
}

void Logic::nextMove() {
    if (historyPos < history.size()) {
        HistoryItem item = history.at(historyPos);

        beginResetModel();
        bool moved = moveInternal(item.fromX, item.fromY, item.toX, item.toY);
        if (moved) {
            historyPos++;
        }
        endResetModel();
        showCurHistoryText();
    }
}

void Logic::prevMove() {
    if (historyPos > 0) {
        historyPos--;
        HistoryItem item = history.at(historyPos);

        beginResetModel();

        Figure* figureTo = getFigureByXY(item.toX, item.toY);
        figureTo->setPos(item.fromX, item.fromY);

        if (item.targetPawnType != pawnNone) {
            impl->figures << Figure { item.targetPawnColor, item.targetPawnType,    item.targetPawnImg,    item.toX, item.toY };
        }

        endResetModel();
        showCurHistoryText();
        nextStep();
    }
}

